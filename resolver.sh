#!/bin/bash
alias lu='dig -r +norecurse +noall +authority +answer +additional "$@"'
alias gr='grep "$1" | head -n 1 | awk "{print $5}"'
function resolve() { # $1: domain, $2: nameserver
    [ "$(lu $2 $1 | tee /tmp/r | grep $1 | gr 'A' | tee /tmp/i)" -n ] && cat /tmp/i} ||
    [ "$(gr 'A' < /tmp/r | grep -v $1 | tee /tmp/g)" -n ] && resolve $1 < /tmp/g ||
    [ "$(gr 'NS' < /tmp/r | tee /tmp/n)" -n ] && resolve $1 $(resolve < /tmp/n '@198.41.0.4') ||
    ( echo 'no ip found' ; exit 1 )
}
resolve $1 '@198.41.0.4'
